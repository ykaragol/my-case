Travel API Client 
=================

Clone this repo and start it (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

To view the assignment (after starting the application) go to:

[http://localhost:9000/travel/index.html](http://localhost:9000/travel/index.html)

What pages do we have:

- [Find Fare](http://localhost:9000/travel/findFare.html)
- [Statistics Dashboard](http://localhost:9000/travel/dashboard.html)
- [Ember Client](http://localhost:9000/travel/dist/index.html)