package com.afkl.cases.df.exception;

import org.springframework.http.HttpStatus;

/**
 * Should be thrown when a dependent service returns an error.
 * 
 */
@SuppressWarnings("serial")
public class ClientHttpException extends RuntimeException {

	private HttpStatus statusCode;

	public ClientHttpException(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

}
