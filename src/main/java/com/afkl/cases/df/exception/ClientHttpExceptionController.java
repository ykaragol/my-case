package com.afkl.cases.df.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Handles every exceptions globally and determines the HttpStatus code for different cases. 
 * 
 * Returns:
 * 	HttpStatus.SERVICE_UNAVAILABLE: When an internal error occurred
 *  HttpStatus.OK: When a dependent service fails
 */
@ControllerAdvice
public class ClientHttpExceptionController {

	private final Logger logger = LoggerFactory.getLogger(ClientHttpExceptionController.class);
	
    @ExceptionHandler(Exception.class)
	public ResponseEntity<String> handleBackEndExceptions(Exception exc) {
    	Throwable cause = exc.getCause()!=null ? exc.getCause() : exc;
		logger.error(cause.getMessage());
		
		ResponseEntity<String> responseEntity;
		if(cause instanceof ClientHttpException){
			responseEntity = new ResponseEntity<>(HttpStatus.OK);
		}else{
			responseEntity = new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
		return responseEntity;
	}

}