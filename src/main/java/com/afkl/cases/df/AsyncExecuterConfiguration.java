package com.afkl.cases.df;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class AsyncExecuterConfiguration {

	@Value("${klm.executer.threadNamePrefix}")
	private String threadNamePrefix;
	
	@Value("${klm.executer.corePoolSize}")
	private Integer corePoolSize;

	@Value("${klm.executer.awaitTerminationSeconds}")
	private Integer awaitTerminationSeconds;
	
	@Bean
	public ThreadPoolTaskExecutor initializeTaskExecuter(){
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		
		taskExecutor.setThreadNamePrefix(threadNamePrefix);
		
		if(corePoolSize != null && corePoolSize > 0){
			taskExecutor.setCorePoolSize(corePoolSize);
		}
		
		if(awaitTerminationSeconds != null && awaitTerminationSeconds > 0){
			taskExecutor.setAwaitTerminationSeconds(awaitTerminationSeconds);
		}
		
		return taskExecutor;
	}
}
