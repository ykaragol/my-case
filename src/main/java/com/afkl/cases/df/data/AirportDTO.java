package com.afkl.cases.df.data;

public class AirportDTO {

	private String name;
	private String code;

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

}
