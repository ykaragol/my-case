package com.afkl.cases.df.data;

public class FareCriterias {

	private String destination;
	private String origin;

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public String getOrigin() {
		return origin;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
