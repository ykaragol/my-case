package com.afkl.cases.df.data;

public class FareAirportDetailDTO {
	private FareDTO fareDTO;
	private AirportDTO destinationAirportDTO;
	private AirportDTO originAirportDTO;

	public FareDTO getFareDTO() {
		return fareDTO;
	}

	public void setFareDTO(FareDTO fareDTO) {
		this.fareDTO = fareDTO;
	}

	public AirportDTO getDestinationAirportDTO() {
		return destinationAirportDTO;
	}

	public void setDestinationAirportDTO(AirportDTO destinationAirportDTO) {
		this.destinationAirportDTO = destinationAirportDTO;
	}

	public AirportDTO getOriginAirportDTO() {
		return originAirportDTO;
	}

	public void setOriginAirportDTO(AirportDTO originAirportDTO) {
		this.originAirportDTO = originAirportDTO;
	}

}
