package com.afkl.cases.df.travelmock;

import org.springframework.web.client.ResponseErrorHandler;

public interface RestTemplateErrorHandler extends ResponseErrorHandler {

}
