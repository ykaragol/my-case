package com.afkl.cases.df.travelmock;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.afkl.cases.df.data.AirportCriterias;
import com.afkl.cases.df.data.AirportDTO;
import com.afkl.cases.df.data.FareCriterias;
import com.afkl.cases.df.data.FareDTO;
import com.afkl.cases.df.dataservices.AirportDataService;
import com.afkl.cases.df.dataservices.FareDataService;
import com.afkl.cases.df.exception.ClientHttpException;
import com.afkl.cases.df.travelmock.data.Fare;
import com.afkl.cases.df.travelmock.data.Location;

/**
 * Common gateway for mock server. Implements both AirportDataService and FareDataService.
 *
 */
@Component
public class TravelMockApiGateway implements AirportDataService, FareDataService {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${klm.mockservice.airportURL}")
	private String airportURL;
	
	@Value("${klm.mockservice.fareURL}")
	private String fareURL;
	
	@Override
	public List<AirportDTO> searchAirports(AirportCriterias criterias) {
		URI uri = generateURI(criterias);
		ResponseEntity<PagedResources<Location>> responseEntity = restTemplate.exchange(
				uri,
				HttpMethod.GET, null, new ParameterizedTypeReference<PagedResources<Location>>() {});
		PagedResources<Location> resources = responseEntity.getBody();
		if(responseEntity.getStatusCode() != HttpStatus.OK){
			throw new ClientHttpException(responseEntity.getStatusCode());
		}
		Location[] locations = resources.getContent().toArray(new Location[0]);
		return convertAirportDTO(locations);
	}

	protected URI generateURI(AirportCriterias criterias) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(airportURL);
		if(criterias.getTerm()!=null) {
			builder.queryParam("term", criterias.getTerm());
		}
		if(criterias.getSize()!=null) {
			builder.queryParam("size", criterias.getSize());
		}
		if(criterias.getLang()!=null) {
			builder.queryParam("lang", criterias.getLang());
		}
		if(criterias.getPage()!=null) {
			builder.queryParam("page", criterias.getPage());
		}
		URI uri = builder.build().encode().toUri();
		return uri;
	}

	protected List<AirportDTO> convertAirportDTO(Location[] locations) {
		List<AirportDTO> airportDTOs = new ArrayList<>(locations.length);
		for(Location location : locations){
			AirportDTO airportDTO = new AirportDTO();
			airportDTO.setName(location.getName());
			airportDTO.setCode(location.getCode());
			airportDTOs.add(airportDTO);
		}
		return airportDTOs;
	}

	@Override
	public FareDTO retrieveFareData(FareCriterias criterias) {
		URI uri = generateURI(criterias);
		ResponseEntity<Fare> responseEntity = restTemplate.exchange(
				uri,
				HttpMethod.GET, null, new ParameterizedTypeReference<Fare>() {});
		if(responseEntity.getStatusCode() != HttpStatus.OK){
			throw new ClientHttpException(responseEntity.getStatusCode());
		}
		Fare fare = responseEntity.getBody();
		return convertFareDTO(fare);
	}

	protected URI generateURI(FareCriterias criterias) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(fareURL);
		builder.pathSegment(criterias.getOrigin());
		builder.pathSegment(criterias.getDestination());
		URI uri = builder.build().encode().toUri();
		return uri;
	}

	protected FareDTO convertFareDTO(Fare fare) {
		FareDTO fareDTO = new FareDTO();
		fareDTO.setAmount(fare.getAmount());
		fareDTO.setCurrency(fare.getCurrency());
		fareDTO.setOrigin(fare.getOrigin());
		fareDTO.setDestination(fare.getDestination());
		return fareDTO;
	}

}
