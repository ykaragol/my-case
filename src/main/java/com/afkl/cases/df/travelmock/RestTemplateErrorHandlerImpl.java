package com.afkl.cases.df.travelmock;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

@Component
public class RestTemplateErrorHandlerImpl implements RestTemplateErrorHandler {
	
	private Logger logger = Logger.getLogger(RestTemplateErrorHandlerImpl.class);

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return (response.getStatusCode() != HttpStatus.OK);
	}

	/*
	 * The reason to override ResponseErrorHandler is to disable throwing an exception when a client exception occured.
	 * So that we can have a flexible control over "responseEntity.getStatusCode()". 
	 * If we decide to throw an exception, we can throw it in gateway.
	 *
	 */
	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		logger.error("status code: "+response.getStatusCode() +" - status text: "+response.getStatusText());
	}

}
