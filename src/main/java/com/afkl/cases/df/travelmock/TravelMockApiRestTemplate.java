package com.afkl.cases.df.travelmock;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Prepares the RestTemplate for mock api.
 */
@Component
public class TravelMockApiRestTemplate {

	@Value("${klm.oauth.accessTokenUri}")
	private String accessTokenUri;

	@Value("${klm.oauth.clientId}")
	private String clientId;

	@Value("${klm.oauth.clientSecret}")
	private String clientSecret;
	
	@Autowired
	private RestTemplateErrorHandler errorHandler;

	public ClientCredentialsResourceDetails createCredentialsResourceDetails() {
		ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
		resource.setAccessTokenUri(accessTokenUri);
		resource.setClientId(clientId);
		resource.setClientSecret(clientSecret);
		return resource;
	}

	@Bean
	public RestTemplate template() {
		ClientCredentialsResourceDetails resource = this.createCredentialsResourceDetails();
		AccessTokenRequest atr = new DefaultAccessTokenRequest();
		OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, new DefaultOAuth2ClientContext(atr));
		MappingJackson2HttpMessageConverter converter = createConverter();
		restTemplate.setMessageConverters(Arrays.asList(converter));
		restTemplate.getAccessToken();
		restTemplate.setRetryBadAccessTokens(true);
		restTemplate.setErrorHandler(errorHandler);
		return restTemplate;
	}

	protected MappingJackson2HttpMessageConverter createConverter() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.registerModule(new Jackson2HalModule());

		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
		converter.setObjectMapper(mapper);
		return converter;
	}

}
