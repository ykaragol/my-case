package com.afkl.cases.df.dataservices;

import java.util.List;

import com.afkl.cases.df.data.AirportDTO;
import com.afkl.cases.df.data.AirportCriterias;

/** 
 * Serves the airport data. 
 */
public interface AirportDataService {
	public List<AirportDTO> searchAirports(AirportCriterias criterias);
}
