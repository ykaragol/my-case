package com.afkl.cases.df.dataservices;

import com.afkl.cases.df.data.FareCriterias;
import com.afkl.cases.df.data.FareDTO;

/** 
 * Serves the fare data. 
 */
public interface FareDataService {

	FareDTO retrieveFareData(FareCriterias criterias);
}
