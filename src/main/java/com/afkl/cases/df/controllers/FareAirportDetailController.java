package com.afkl.cases.df.controllers;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.data.AirportCriterias;
import com.afkl.cases.df.data.AirportDTO;
import com.afkl.cases.df.data.FareAirportDetailDTO;
import com.afkl.cases.df.data.FareCriterias;
import com.afkl.cases.df.data.FareDTO;
import com.afkl.cases.df.dataservices.AirportDataService;
import com.afkl.cases.df.dataservices.FareDataService;

@RestController
public class FareAirportDetailController {

	@Autowired
	private AirportDataService airportDataService;
	
	@Autowired
	private FareDataService fareDataService;
	
	@Autowired 
	private ThreadPoolTaskExecutor taskExecutor;
	

	@RequestMapping("/retrieveFareWithAirport")
	public FareAirportDetailDTO retrieveFareWithAirports(@RequestParam String origin, @RequestParam String destination) throws InterruptedException, ExecutionException {
		Future<FareDTO> fareFuture = startRetrievingFare(origin, destination);
		Future<List<AirportDTO>> originAirportListFuture = startRetrievingAirport(origin);
		Future<List<AirportDTO>> destinationAirportListFuture = startRetrievingAirport(destination);

		FareAirportDetailDTO fareAirportDetailDTO = new FareAirportDetailDTO();
		
		fareAirportDetailDTO.setOriginAirportDTO(findAirportWithCode(originAirportListFuture.get(), origin));
		fareAirportDetailDTO.setDestinationAirportDTO(findAirportWithCode(destinationAirportListFuture.get(), destination));
		fareAirportDetailDTO.setFareDTO(fareFuture.get());
		
		return fareAirportDetailDTO;
	}


	private AirportDTO findAirportWithCode(List<AirportDTO> list, String code) {
		return list.stream().filter(item->code.equals(item.getCode())).findAny().get();
	}


	private Future<List<AirportDTO>> startRetrievingAirport(String airportCode) {
		AirportCriterias airportCriterias = new AirportCriterias();
		airportCriterias.setTerm(airportCode);	
		
		Future<List<AirportDTO>> airportList = taskExecutor.submit(()->{
			return airportDataService.searchAirports(airportCriterias);
		});
		return airportList;
	}


	private Future<FareDTO> startRetrievingFare(String origin, String destination) {
		FareCriterias fareCriterias = new FareCriterias();
		fareCriterias.setOrigin(origin);
		fareCriterias.setDestination(destination);
		
		Future<FareDTO> fare = taskExecutor.submit(()->{
			return fareDataService.retrieveFareData(fareCriterias);
		});
		return fare;
	}

}
