package com.afkl.cases.df.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.data.FareCriterias;
import com.afkl.cases.df.data.FareDTO;
import com.afkl.cases.df.dataservices.FareDataService;

@RestController
public class FareInfoController {

	@Autowired
	private FareDataService fareDataService;

	@RequestMapping("/retrieveFare")
	public FareDTO retrieveFare(@RequestParam String origin, @RequestParam String destination) {
		FareCriterias criterias = new FareCriterias();
		criterias.setOrigin(origin);
		criterias.setDestination(destination);
		
		return fareDataService.retrieveFareData(criterias);
	}
}
