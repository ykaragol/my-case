package com.afkl.cases.df.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.data.AirportCriterias;
import com.afkl.cases.df.data.AirportDTO;
import com.afkl.cases.df.dataservices.AirportDataService;

@RestController
public class AirportInfoController {

	@Autowired
	private AirportDataService airportDataService;

	@RequestMapping("/searchAirports")
	public List<AirportDTO> searchAirports(@RequestParam String term, @RequestParam String lang,
			@RequestParam(required=false) Integer page, @RequestParam(required=false) Integer size) {
		AirportCriterias criterias = new AirportCriterias();
		criterias.setTerm(term);
		criterias.setLang(lang);
		criterias.setPage(page);
		criterias.setSize(size);

		return airportDataService.searchAirports(criterias);
	}

}
