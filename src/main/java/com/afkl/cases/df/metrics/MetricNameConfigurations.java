package com.afkl.cases.df.metrics;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MetricNameConfigurations {
	
	@Value("${klm.metric.parameters.totalRequest}")
	public String totalRequestCounterName;
	
	@Value("${klm.metric.parameters.okRequestCounter}")
	public String okRequestCounterName;
	
	@Value("${klm.metric.parameters.http4xxRequestCounter}")
	public String http4xxRequestCounterName;

	@Value("${klm.metric.parameters.http5xxRequestCounter}")
	public String http5xxRequestCounterName;

	@Value("${klm.metric.parameters.responseTimer}")
	public String responseTimerName;
}
