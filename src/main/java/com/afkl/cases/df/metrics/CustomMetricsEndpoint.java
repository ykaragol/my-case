package com.afkl.cases.df.metrics;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import org.springframework.boot.actuate.endpoint.MetricsEndpoint;
import org.springframework.stereotype.Component;

@Component
public class CustomMetricsEndpoint extends AbstractEndpoint<Map<String, Object>> {

    private MetricsEndpoint metricsEndpoint;
   
    private List<String> metricNameList; 
    
    @Autowired
    public CustomMetricsEndpoint(MetricsEndpoint metricsEndpoint, MetricNameConfigurations metricNameConfigurations, @Value("${klm.metric.endPoint}") String metricEndPoint) {
        super(metricEndPoint);
        this.metricsEndpoint = metricsEndpoint;
        
        this.metricNameList = Arrays.asList(
        		metricNameConfigurations.responseTimerName+".snapshot.mean",
        		metricNameConfigurations.responseTimerName+".snapshot.min",
        		metricNameConfigurations.responseTimerName+".snapshot.max",
        		metricNameConfigurations.http4xxRequestCounterName,
        		metricNameConfigurations.http5xxRequestCounterName,
        		metricNameConfigurations.okRequestCounterName,
        		metricNameConfigurations.totalRequestCounterName
        		);
    }

    @Override
    public Map<String, Object> invoke() {
        Map<String, Object> allMetrics = this.metricsEndpoint.invoke();

        return allMetrics.entrySet().stream()
        		.filter(item -> metricNameList.contains(item.getKey()))
        		.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));        
    }

}
