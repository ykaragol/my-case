package com.afkl.cases.df.metrics;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

@Component
public class MetricsFilter implements Filter {

	@Autowired
	private MetricRegistry metricRegistry;
	
	@Autowired
	private MetricNameConfigurations metricNameConfigurations;

	private Counter totalRequestCounter;
	private Counter okRequestCounter;
	private Counter http4xxRequestCounter;
	private Counter http5xxRequestCounter;
	private Timer responseTimer;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		final Timer.Context context = responseTimer.time();
		try {
			chain.doFilter(request, response);
		} finally {
			context.stop();
			writeStatusCounters(response);
		}
	}

	private void writeStatusCounters(ServletResponse response) {
		totalRequestCounter.inc();

		int status = ((HttpServletResponse) response).getStatus();
		if (status < 400) {
			okRequestCounter.inc();
		} else if (status < 500) {
			http4xxRequestCounter.inc();
		} else {
			http5xxRequestCounter.inc();
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		totalRequestCounter = metricRegistry.counter(metricNameConfigurations.totalRequestCounterName);
		okRequestCounter = metricRegistry.counter(metricNameConfigurations.okRequestCounterName);
		http4xxRequestCounter = metricRegistry.counter(metricNameConfigurations.http4xxRequestCounterName);
		http5xxRequestCounter = metricRegistry.counter(metricNameConfigurations.http5xxRequestCounterName);
		responseTimer = metricRegistry.timer(metricNameConfigurations.responseTimerName);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
