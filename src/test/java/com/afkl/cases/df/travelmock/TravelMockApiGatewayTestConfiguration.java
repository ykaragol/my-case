package com.afkl.cases.df.travelmock;

import java.util.Properties;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

@ContextConfiguration
public class TravelMockApiGatewayTestConfiguration {

	@Bean
	public PropertySourcesPlaceholderConfigurer properties() throws Exception {
		final PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
		Properties properties = new Properties();

		properties.setProperty("klm.mockservice.airportURL", "http://dummy1");
		properties.setProperty("klm.mockservice.fareURL", "http://dummy2");

		pspc.setProperties(properties);
		return pspc;
	}

	@Bean
	public RestTemplate instantiateRestTemplate() {
		return Mockito.mock(RestTemplate.class);
	}

	@Bean
	public TravelMockApiGateway instantiateTravelMockApiGateway() {
		return new TravelMockApiGateway();
	}

}
