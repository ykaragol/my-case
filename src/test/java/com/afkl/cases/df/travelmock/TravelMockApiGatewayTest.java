package com.afkl.cases.df.travelmock;

import java.net.URI;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.afkl.cases.df.data.AirportCriterias;
import com.afkl.cases.df.data.FareCriterias;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={TravelMockApiGatewayTestConfiguration.class})
public class TravelMockApiGatewayTest {

	@Autowired
	private TravelMockApiGateway travelApiGateway;
	
	@Value("${klm.mockservice.airportURL}")
	private String airportURL;
	
	@Value("${klm.mockservice.fareURL}")
	private String fareURL;
	
	
	@Test
	public void testURIGenerationForAirport() {
		//setup
		AirportCriterias criterias = new AirportCriterias();
		criterias.setLang("lang1");
		criterias.setPage(15);
		criterias.setSize(20);
		criterias.setTerm("term1");
		
		//act
		URI uri = travelApiGateway.generateURI(criterias);
		
		//asserts:
		Assert.assertEquals("", uri.getPath());
		Assert.assertEquals(airportURL, "http://" + uri.getHost());
		String query = uri.getQuery();
		Assert.assertNotNull(query);
		List<String> queries = Arrays.asList(query.split("&"));
		Assert.assertEquals(4, queries.size());
		List<String> original = new LinkedList<String>(){
			{
				add("term=term1");
				add("lang=lang1");
				add("page=15");
				add("size=20");
			}
		};
		Assert.assertTrue(queries.containsAll(original));
	}
	
	@Test
	public void testURIGenerationForFare() {
		//setup
		FareCriterias criterias = new FareCriterias();
		criterias.setDestination("destination2");
		criterias.setOrigin("origin3");
		
		//act
		URI uri = travelApiGateway.generateURI(criterias);
		
		//asserts:
		Assert.assertEquals("/origin3/destination2", uri.getPath());
		Assert.assertEquals(fareURL, "http://" + uri.getHost());

	}

}
