package com.afkl.cases.df.controllers;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.afkl.cases.df.AsyncExecuterConfiguration;
import com.afkl.cases.df.data.AirportCriterias;
import com.afkl.cases.df.data.AirportDTO;
import com.afkl.cases.df.data.FareCriterias;
import com.afkl.cases.df.data.FareDTO;
import com.afkl.cases.df.dataservices.AirportDataService;
import com.afkl.cases.df.dataservices.FareDataService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={FareAirportDetailControllerTestConfiguration.class, AsyncExecuterConfiguration.class})
public class FareAirportDetailControllerTest {
	
	@Autowired
	private FareAirportDetailController controller;

	@Autowired
	private AirportDataService airportDataServiceMock;
	
	@Autowired
	private FareDataService fareDataServiceMock;
	
	
	@Test
	public void testConcurrencyConfiguration() throws InterruptedException, ExecutionException {
	
		final long waitTime = 10000L;
		
		/* Arrange step: Prepare data for test */
		Mockito.when(airportDataServiceMock.searchAirports(Mockito.any(AirportCriterias.class)))
			.then(new Answer<List<AirportDTO>>() {
				@Override
				public List<AirportDTO> answer(InvocationOnMock invocation) throws Throwable {
					Thread.sleep(waitTime);
					List<AirportDTO> list = new LinkedList<>();
					AirportDTO origin = new AirportDTO();
					origin.setCode("orig");
					list.add(origin);
					AirportDTO destination = new AirportDTO();
					destination.setCode("dest");
					list.add(destination);
					return list;
				}
		});
		
		Mockito.when(fareDataServiceMock.retrieveFareData(Mockito.any(FareCriterias.class)))
			.then(new Answer<FareDTO>() {
				@Override
				public FareDTO answer(InvocationOnMock invocation) throws Throwable {
					Thread.sleep(waitTime);
					return new FareDTO();
				}
			});
		
		/* Act step: do run */
		long start = System.currentTimeMillis();
		controller.retrieveFareWithAirports("orig", "dest");
		long stop = System.currentTimeMillis();
		
		/* Assert step: verify results */
		long timeTook = stop - start;
		Assert.assertTrue("Concurrent call exceed time!", timeTook > 10000);
		Assert.assertTrue("Concurrent call exceed time!", timeTook < 10000*2);
	}

}
