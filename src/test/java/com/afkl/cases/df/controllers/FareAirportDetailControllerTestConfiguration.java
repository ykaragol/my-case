package com.afkl.cases.df.controllers;

import java.util.Properties;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.ContextConfiguration;

import com.afkl.cases.df.dataservices.AirportDataService;
import com.afkl.cases.df.dataservices.FareDataService;

@ContextConfiguration
public class FareAirportDetailControllerTestConfiguration {

	@Bean
	public FareAirportDetailController instantiateFareAirportDetailController(){
		return new FareAirportDetailController();
	}

	@Bean
	public AirportDataService instantiateAirportDataService(){
		return Mockito.mock(AirportDataService.class);
	}
	
	@Bean
	public FareDataService instantiateFareDataService(){
		return Mockito.mock(FareDataService.class);		
	}

    @Bean
    public PropertySourcesPlaceholderConfigurer properties() throws Exception {
    	//From: http://stackoverflow.com/questions/17353327/populating-spring-value-during-unit-test
        final PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
        Properties properties = new Properties();

        properties.setProperty("klm.executer.corePoolSize", "3");
        properties.setProperty("klm.executer.awaitTerminationSeconds", "60");
        properties.setProperty("klm.executer.threadNamePrefix", "prefix");

        pspc.setProperties(properties);
        return pspc;
    }
	
}
